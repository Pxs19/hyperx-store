/*
  Warnings:

  - A unique constraint covering the columns `[categoryId]` on the table `Products` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Products_categoryId_key" ON "Products"("categoryId");
