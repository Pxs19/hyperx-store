/*
  Warnings:

  - A unique constraint covering the columns `[cartId]` on the table `Products` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Products_cartId_key" ON "Products"("cartId");
