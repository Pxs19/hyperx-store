/*
  Warnings:

  - You are about to drop the column `cartId` on the `Products` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Products" DROP CONSTRAINT "Products_cartId_fkey";

-- DropIndex
DROP INDEX "Products_cartId_key";

-- AlterTable
ALTER TABLE "Products" DROP COLUMN "cartId";
