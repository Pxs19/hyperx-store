import { Injectable } from '@nestjs/common';
import express from 'express';
import { Response } from 'express';

@Injectable()
export abstract class BaseController {
  protected jsonResponse(
    res: Response,
    code: number,
    message: string,
    ok: boolean,
    data: any,
  ) {
    return res.status(code).json({
      ok,
      message,
      data,
    });
  }

  // protected jsonResponse(
  //   res: Response,
  //   code: number,
  //   message: string,
  //   ok: boolean,
  // ) {
  //   return res.status(code).json({ ok, message });
  // }

  protected successResponse(data: any) {
    return {
      status: 'success',
      data,
    };
  }

  protected signinupResponse(
    res: Response,
    code: number,
    ok: boolean,
    message: string,
    tokens: {},
  ) {
    return res.status(code).json({
      ok,
      message,
      tokens,
    });
  }

  protected errorAuthResponse(
    res: Response,
    code: number,
    ok: boolean,
    message: string,
  ) {
    return res.status(code).json({
      ok,
      message,
    });
  }

  protected signoutResponse(
    res: Response,
    code: number,
    ok: boolean,
    message: string,
  ) {
    return res.status(code).json({
      ok,
      message,
    });
  }
}
