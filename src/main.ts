import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import {
  CorsConfig,
  NestConfig,
  SwaggerConfig,
} from './configs/configs.interface';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const nestConfig = configService.get<NestConfig>('nest');
  const corsConfig = configService.get<CorsConfig>('cors');
  const swaggerConfig = configService.get<SwaggerConfig>('swagger');

  app.useGlobalPipes(new ValidationPipe());

  console.log(process.env.DATABASE_URL);
  // console.log(process.env.SWAGGER_PATH);

  // swagger
  if (swaggerConfig.enabled) {
    const config = new DocumentBuilder()
      .setTitle(swaggerConfig.title)
      .setDescription(swaggerConfig.description)
      .setVersion(swaggerConfig.version)
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup(swaggerConfig.path, app, document);

    // console.log('ssss' + swaggerConfig.path);
  }

  // CORS
  if (corsConfig.enabled) {
    app.enableCors({
      credentials: corsConfig.credentials,
      origin: '*',
    });
  }

  await app.listen(nestConfig.port);
  Logger.log(
    `[${Logger.getTimestamp()}] Starting server on port ${nestConfig.port}`,
  );
}
bootstrap();
