export interface CorsConfig {
  enabled: boolean;
  credentials: boolean;
}

export interface SwaggerConfig {
  enabled: boolean;
  title: string;
  description: string;
  version: string;
  path: string;
}

export interface NestConfig {
  env: string;
  port: number;
}

export interface Config {
  nest: NestConfig;
  swagger: SwaggerConfig;
  cors: CorsConfig;
}
