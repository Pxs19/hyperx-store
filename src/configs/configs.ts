import { Config } from './configs.interface';

export default (): Config => ({
  nest: {
    env: process.env.NODE_ENV,
    port: Number(process.env.PORT),
  },

  // jj s

  swagger: {
    enabled: Boolean(process.env.SWAGGER_ENABLED),
    title: process.env.SWAGGER_TITLE,
    description: process.env.SWAGGER_DESCRIPTION,
    version: process.env.SWAGGER_VERSION,
    path: process.env.SWAGGER_PATH,
  },
  cors: {
    enabled: Number(process.env.ENABLE_CORS) === 1 || true,
    credentials: Number(process.env.ENABLE_CREDENTIAL) === 1 || true,
  },
});
