import { Inject } from '@nestjs/common';
import { CreateCategoryDto } from 'src/api/category/dto/create-category.dto';
import { PrismaService } from 'src/prisma/prisma.service';

export class CategoryRepository {
  constructor(@Inject(PrismaService) readonly prisma: PrismaService) {}

  async createCatogory(data: CreateCategoryDto) {
    const result = await this.prisma.category.create({
      data: { category_name: data.category_name },
    });

    return result;
  }

  async findCategory(category_name: string) {
    const result = await this.prisma.category.findUnique({
      where: { category_name },
    });

    return result;
  }
}
