import { Inject } from '@nestjs/common';
import { AuthSignUp } from 'src/api/auth/dto';
import { PrismaService } from 'src/prisma/prisma.service';

export class UserReposity {
  constructor(@Inject(PrismaService) readonly prisma: PrismaService) {}

  async findUsername(username: string) {
    const result = await this.prisma.user.findUnique({ where: { username } });

    return result;
  }

  async findEmail(email: string) {
    const result = await this.prisma.user.findUnique({ where: { email } });

    return result;
  }

  async addUser(data: AuthSignUp) {
    const result = await this.prisma.user.create({
      data: {
        username: data.username,
        password: data.password,
        email: data.email,
      },
    });

    return result;
  }

  async updateRtHash(userId: string, rtHash: string) {
    const result = await this.prisma.user.update({
      where: { id: userId },
      data: {
        rtHash: rtHash,
      },
    });
  }

  async removeRt(userId: string) {
    await this.prisma.user.updateMany({
      where: {
        id: userId,
        rtHash: {
          not: null,
        },
      },
      data: {
        rtHash: null,
      },
    });
  }
}
