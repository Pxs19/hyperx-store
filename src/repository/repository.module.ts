import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UserReposity } from './user.repository/user.repository';
import { ProductsReposity } from './products/products.repository';
import { CategoryRepository } from './category/category.repository';

const service = [UserReposity, ProductsReposity, CategoryRepository];

@Module({
  imports: [PrismaModule],
  providers: service,
  exports: service,
})
export default class Repository {}
