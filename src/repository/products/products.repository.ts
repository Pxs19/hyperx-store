import { Inject } from '@nestjs/common';
import { CreateProductDto } from 'src/api/products/dto/create-product.dto';

import { PrismaService } from 'src/prisma/prisma.service';

export class ProductsReposity {
  constructor(@Inject(PrismaService) readonly prisma: PrismaService) {}

  async getProducts() {
    const result = await this.prisma.products.findMany();

    return result;
  }

  async addProduct(data: CreateProductDto) {
    const { product_name, product_desc, product_category, price, quantity } =
      data;

    const result = await this.prisma.products.create({
      data: {
        product_name,
        product_desc,
        price,
        quantity,
        categoryId: product_category,
      },
    });

    return result;

    // console.log(product_category);
  }
}
