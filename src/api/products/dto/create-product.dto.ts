import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty } from 'class-validator';

export class CreateProductDto {
  @ApiProperty({})
  @IsNotEmpty()
  product_name: string;

  @ApiProperty({})
  @IsNotEmpty()
  product_desc: string;

  @ApiProperty({})
  product_category: string;

  @ApiProperty({})
  @IsNotEmpty()
  price: number;

  @ApiProperty({})
  @IsNotEmpty()
  quantity: number;
}
