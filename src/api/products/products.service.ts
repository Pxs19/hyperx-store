import { Inject, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductsReposity } from 'src/repository/products/products.repository';
import { CategoryRepository } from 'src/repository/category/category.repository';

@Injectable()
export class ProductsService {
  constructor(
    @Inject(ProductsReposity)
    private readonly productRepository: ProductsReposity,
    @Inject(CategoryRepository)
    private readonly categoryRepository: CategoryRepository,
  ) {}

  async create(data: CreateProductDto) {
    // get categoryId
    data.product_category = data.product_category.toLowerCase();
    const categoryId = await this.categoryRepository.findCategory(
      data.product_category,
    );

    if (categoryId != null) {
      data.product_category = categoryId.id;
      return await this.productRepository.addProduct(data);
    } else {
      return { message: 'no category' };
    }
  }

  async findAll() {
    return await this.productRepository.getProducts();
  }

  findOne(id: number) {
    return `This action returns a #${id} product`;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  remove(id: number) {
    return `This action removes a #${id} product`;
  }
}
