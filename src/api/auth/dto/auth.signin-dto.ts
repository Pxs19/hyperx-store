import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class AuthSignIn {
  @ApiProperty({ description: 'username' })
  @IsNotEmpty()
  username: string;

  @ApiProperty({ description: 'password' })
  @IsNotEmpty()
  password: string;
}
