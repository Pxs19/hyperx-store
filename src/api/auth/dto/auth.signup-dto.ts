import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class AuthSignUp {
  @ApiProperty({ description: 'username' })
  @IsNotEmpty()
  username: string;

  @ApiProperty({ description: 'password' })
  @IsNotEmpty()
  password: string;

  @ApiProperty({ description: 'email' })
  @IsEmail()
  @IsNotEmpty()
  email: string;
}
