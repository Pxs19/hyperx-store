import {
  ConflictException,
  ForbiddenException,
  Inject,
  Injectable,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Tokens } from './token';
import { AuthSignIn, AuthSignUp } from './dto';
import { UserReposity } from 'src/repository/user.repository/user.repository';
import HashData from 'src/utils/hash';
// import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @Inject(UserReposity) private readonly userRepository: UserReposity,
    private hashData: HashData,
  ) {}

  async getToken(userId: string, username: string): Promise<Tokens> {
    const [access_token, refresh_token] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
        },
        {
          secret: 'at-secret',
          expiresIn: 60 * 15,
        },
      ),
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
        },
        {
          secret: 'at-secret',
          expiresIn: 60 * 15,
        },
      ),
    ]);

    return { access_token, refresh_token };
  }

  async updateRtHash(userId: string, rtHash: string) {
    const rtHashed = await this.hashData.hash(rtHash);

    await this.userRepository.updateRtHash(userId, rtHashed);
  }

  async registerUser(data: AuthSignUp) {
    let { username, password, email } = data;

    // check username and email
    const have_username = await this.userRepository.findUsername(username);
    const have_email = await this.userRepository.findEmail(email);
    // console.log('ss' + have_username);

    // note: if username or email not use -> will return "null"
    if (have_username != null)
      throw new ConflictException(`${username} already exist`);
    if (have_email != null)
      throw new ConflictException(`${email} already exist`);

    // hash-password
    const hashPassword = await this.hashData.hash(password);

    // add data to db
    const payload: AuthSignUp = {
      username,
      password: hashPassword,
      email,
    };
    const user = await this.userRepository.addUser(payload);

    // get token
    const tokens = this.getToken(user.id, user.username);

    // update rthash
    await this.updateRtHash(user.id, (await tokens).refresh_token);

    return tokens;
  }

  async signinUser(signInData: AuthSignIn) {
    const { username, password } = signInData;

    // find username
    const user = await this.userRepository.findUsername(username);

    if (user == null)
      throw new ForbiddenException(`${username} not found, access denied !!`);

    // check Password
    const checkPassword = await this.hashData.compareHash(
      password,
      user.password,
    );

    if (!checkPassword)
      throw new ForbiddenException(`password not match, access denied !!`);

    // get token
    const tokens = this.getToken(user.id, user.username);

    // update rthash
    await this.updateRtHash(user.id, (await tokens).refresh_token);

    return tokens;
  }

  async signout(userId: string) {
    await this.userRepository.removeRt(userId);
  }
}
