import { Body, Controller, Post, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthSignIn, AuthSignUp } from './dto';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { Request } from 'express';
import { AtGuard } from './guard';
import { BaseController } from 'src/utils/base.controller';
import { Roles } from '../role/decorators/role.decorator';
import { RolesGuard } from '../role/guards/roles.guard';
import { Role } from '../role/enum.role/role.enum';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController extends BaseController {
  constructor(private authService: AuthService) {
    super();
  }
  @Post('/signup')
  async register(
    @Req() req: Request,
    @Res() res: Response,
    @Body() signUpData: AuthSignUp,
  ) {
    try {
      const result = await this.authService.registerUser(signUpData);
      return this.signinupResponse(
        res,
        201,
        true,
        'User signup successful !',
        result,
      );
    } catch (error) {
      return res.send(error);
    }
  }

  @Post('/signin')
  async signin(
    @Req() req: Request,
    @Res() res: Response,
    @Body() signInData: AuthSignIn,
  ) {
    try {
      const result = await this.authService.signinUser(signInData);

      return this.signinupResponse(
        res,
        200,
        true,
        'User signin successful !',
        result,
      );
    } catch (error) {
      // return this.errorAuthResponse(res, 403, false, "");
      return res.json(error);
    }
  }

  @ApiBearerAuth()
  @UseGuards(AtGuard)
  @Post('/signout')
  async signout(@Req() req: Request, @Res() res: Response) {
    try {
      const user = req.user;
      await this.authService.signout(user['sub']);

      return this.signoutResponse(res, 200, true, 'signout successful !!');
    } catch (error) {
      return res.json(error);
    }
  }

  @UseGuards(AtGuard, RolesGuard)
  @Roles(Role.USER)
  @ApiBearerAuth()
  @Post('/test')
  async testGuard(@Req() req: Request, @Res() res: Response) {
    const user = req.user;
    console.log(user['sub'], user['username']);

    return res.send('ss');
  }
}
