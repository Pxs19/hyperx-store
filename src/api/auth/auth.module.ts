import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { AuthController } from './auth.controller';
import Repository from 'src/repository/repository.module';
import AtStrategy from './strategy/at-strategy';
import RtStrategy from './strategy/rt-strategy';
import { JwtModule } from '@nestjs/jwt';
import HashData from 'src/utils/hash';

@Module({
  imports: [PrismaModule, Repository, JwtModule.register({})],
  providers: [AuthService, AtStrategy, RtStrategy, HashData],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
