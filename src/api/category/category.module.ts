import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import Repository from 'src/repository/repository.module';

@Module({
  controllers: [CategoryController],
  providers: [CategoryService],
  imports: [Repository],
})
export class CategoryModule {}
